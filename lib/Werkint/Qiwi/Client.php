<?php

namespace Werkint\Qiwi;

use Werkint\Qiwi\Status as S;

class Client {

    protected $login;
    protected $password;
    const PUT='PUT';
    const PATCH='PATCH';
    const GET='GET';
    public function __construct($login, $password) {
        $this->login = $login;
        $this->password = $password;
    }

    public function createBill($phone, $amount, $bill_id, $desc, $prv_name, $create = true) {
        $url = "https://w.qiwi.com/api/v2/prv/{$this->login}/bills/{$bill_id}";
        $datetime = new \DateTime(date("d.m.Y H:i:s", strtotime("+2 weeks")));
        $fields = array(
            'user' => 'tel:' . $phone,
            'amount' => $amount,
            'ccy' => 'RUB',
            'comment' => $desc,
            'lifetime' => $datetime->format(\DateTime::ISO8601),
            'prv_name'=>$prv_name
        );
        $res = $this->doRequest(self::PUT, $url, $fields);
        $res = \CJSON::decode($res);
        return new S\StatusResult($res['response']['result_code']);
    }

    public function cancelBill($bill_id) {
        $url = "https://w.qiwi.com/api/v2/prv/{$this->login}/bills/{$bill_id}";
        $fields = array(
            'status' => 'rejected'
        );
        $res = $this->doRequest(self::PATCH, $url, $fields);
        $res = \CJSON::decode($res);
        return new S\StatusResult($res['response']['result_code']);
    }
    public function getStatus($bill_id) {
        $url = "https://w.qiwi.com/api/v2/prv/{$this->login}/bills/{$bill_id}";
        $fields = array();
        $res = $this->doRequest(self::GET, $url, $fields);
        $res = \CJSON::decode($res);
        return $res['response'];
    }
    private function doRequest($type, $url, $fields) {
        $fields = (is_array($fields)) ? http_build_query($fields) : $fields;
        if (($ch = curl_init($url))) {
            $res = base64_encode($this->login . ':' . $this->password);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Accept: text/json',
                'Authorization: Basic ' . $res
            ));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        } else {
            return false;
        }
    }
    public function getRedirectUrl($txn_id, $successUrl=null, $failUrl=null) {
        $str = "https://w.qiwi.com/order/external/main.action?shop={$this->login}&transaction={$txn_id}";
        if($successUrl)
            $str .= "&successUrl={$successUrl}";
        if($failUrl)
            $str .= "&failUrl={$failUrl}";
        return $str;
    }

}

